<?php
namespace ejemplo9;// recordales en que carpeta esta.
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of 9
 *
 * @author Programacion
 */
class  Circulo{
    
    private $radio;
    private $centrox;
    private $centroy;
    
    public $area;
    public $perimetro;
    
    public $pinta;
    
    function __construct($radio=50, $centrox= 25, $centroy=25) {
        
        $this->radio = $radio;
        $this->centrox = $centrox;
        $this->centroy = $centroy;
    }


    public function getRadio() {
        return $this->radio;
    }

    public function getCentrox() {
        return $this->centrox;
    }

    public function getCentroy() {
        return $this->centroy;
    }

    public function getArea() {
        return $this->area;
    }

    public function getPerimetro() {
        return $this->perimetro;
    }

    public function setRadio($radio): void {
        $this->radio = $radio;
    }

    public function setCentrox($centrox): void {
        $this->centrox = $centrox;
    }

    public function setCentroy($centroy): void {
        $this->centroy = $centroy;
    }

    public function setArea($area): void {
        $this->area = $area;
    }

    public function setPerimetro($perimetro): void {
        $this->perimetro = $perimetro;
    }

    public function Area() {
        
        $resultado=0;
        $resultado= pi()*$this->radio**2;
        return $resultado;
        
    }
    
    public function Perimetro (){
         
        $resultado=0;
        $resultado=2*pi()*$this->radio;
        return $resultado;
        
        
    }

    public function pintar(){
        
  echo '<svg height="100" width="100">';
  echo '<circle cx="'.$this->centrox.'" cy="'.$this->centroy.'" r="'.$this->radio.'" stroke="black" stroke-width="3" fill="red" />';
   echo '</svg>';
        
    }
}
