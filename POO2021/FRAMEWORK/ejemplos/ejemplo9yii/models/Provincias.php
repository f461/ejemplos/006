<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "provincias".
 *
 * @property int $codigo
 * @property string|null $nombre
 *
 * @property Destinario[] $destinarios
 * @property Paquete[] $paquetes
 */
class Provincias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'provincias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Destinarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDestinarios()
    {
        return $this->hasMany(Destinario::className(), ['provincia' => 'codigo']);
    }

    /**
     * Gets query for [[Paquetes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaquetes()
    {
        return $this->hasMany(Paquete::className(), ['codigo' => 'paquetes'])->viaTable('destinario', ['provincia' => 'codigo']);
    }
}
