<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "destinario".
 *
 * @property int $provincia
 * @property int $paquetes
 *
 * @property Paquete $paquetes0
 * @property Provincias $provincia0
 */
class destinatarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'destinario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['provincia', 'paquetes'], 'required'],
            [['provincia', 'paquetes'], 'integer'],
            [['paquetes'], 'unique'],
            [['provincia', 'paquetes'], 'unique', 'targetAttribute' => ['provincia', 'paquetes']],
            [['paquetes'], 'exist', 'skipOnError' => true, 'targetClass' => Paquete::className(), 'targetAttribute' => ['paquetes' => 'Codigo']],
            [['provincia'], 'exist', 'skipOnError' => true, 'targetClass' => Provincias::className(), 'targetAttribute' => ['provincia' => 'Codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'provincia' => 'Provincia',
            'paquetes' => 'Paquetes',
        ];
    }

    /**
     * Gets query for [[Paquetes0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaquetes0()
    {
        return $this->hasOne(Paquete::className(), ['Codigo' => 'paquetes']);
    }

    /**
     * Gets query for [[Provincia0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvincia0()
    {
        return $this->hasOne(Provincias::className(), ['Codigo' => 'provincia']);
    }
}
