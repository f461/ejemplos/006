<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "camioneros".
 *
 * @property string $dni
 * @property string|null $nombre
 * @property string|null $direccion
 * @property string|null $telefono
 * @property string|null $poblacion
 *
 * @property Camion[] $camions
 * @property Conduce[] $conduces
 * @property Distribuye[] $distribuyes
 * @property Paquete[] $paquetes
 */
class Camioneros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'camioneros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni'], 'required'],
            [['dni'], 'string', 'max' => 15],
            [['nombre', 'poblacion'], 'string', 'max' => 255],
            [['direccion', 'telefono'], 'string', 'max' => 20],
            [['dni'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'direccion' => 'Direccion',
            'telefono' => 'Telefono',
            'poblacion' => 'Poblacion',
        ];
    }

    /**
     * Gets query for [[Camions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCamions()
    {
        return $this->hasMany(Camion::className(), ['matricula' => 'camion'])->viaTable('conduce', ['camioneros' => 'dni']);
    }

    /**
     * Gets query for [[Conduces]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConduces()
    {
        return $this->hasMany(Conduce::className(), ['camioneros' => 'dni']);
    }

    /**
     * Gets query for [[Distribuyes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDistribuyes()
    {
        return $this->hasMany(Distribuye::className(), ['camioneros' => 'dni']);
    }

    /**
     * Gets query for [[Paquetes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaquetes()
    {
        return $this->hasMany(Paquete::className(), ['codigo' => 'paquete'])->viaTable('distribuye', ['camioneros' => 'dni']);
    }
}
