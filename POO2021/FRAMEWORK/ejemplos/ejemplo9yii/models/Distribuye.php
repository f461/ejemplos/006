<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "distribuye".
 *
 * @property string $camioneros
 * @property int $paquete
 *
 * @property Camioneros $camioneros0
 * @property Paquete $paquete0
 */
class Distribuye extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'distribuye';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['camioneros', 'paquete'], 'required'],
            [['paquete'], 'integer'],
            [['camioneros'], 'string', 'max' => 15],
            [['paquete'], 'unique'],
            [['camioneros', 'paquete'], 'unique', 'targetAttribute' => ['camioneros', 'paquete']],
            [['paquete'], 'exist', 'skipOnError' => true, 'targetClass' => Paquete::className(), 'targetAttribute' => ['paquete' => 'codigo']],
            [['camioneros'], 'exist', 'skipOnError' => true, 'targetClass' => Camioneros::className(), 'targetAttribute' => ['camioneros' => 'dni']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'camioneros' => 'Camioneros',
            'paquete' => 'Paquete',
        ];
    }

    /**
     * Gets query for [[Camioneros0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCamioneros0()
    {
        return $this->hasOne(Camioneros::className(), ['dni' => 'camioneros']);
    }

    /**
     * Gets query for [[Paquete0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaquete0()
    {
        return $this->hasOne(Paquete::className(), ['codigo' => 'paquete']);
    }
}
