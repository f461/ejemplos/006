<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "paquete".
 *
 * @property int $Codigo
 * @property string|null $Descripcion
 * @property string|null $Destinatario
 * @property string|null $direcion
 *
 * @property Destinario $destinario
 * @property Provincias[] $provincias
 */
class paquetes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paquete';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Codigo'], 'required'],
            [['Codigo'], 'integer'],
            [['Descripcion', 'Destinatario', 'direcion'], 'string', 'max' => 255],
            [['Codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Codigo' => 'Codigo',
            'Descripcion' => 'Descripcion',
            'Destinatario' => 'Destinatario',
            'direcion' => 'Direcion',
        ];
    }

    /**
     * Gets query for [[Destinario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDestinario()
    {
        return $this->hasOne(Destinario::className(), ['paquetes' => 'Codigo']);
    }

    /**
     * Gets query for [[Provincias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvincias()
    {
        return $this->hasMany(Provincias::className(), ['Codigo' => 'provincia'])->viaTable('destinario', ['paquetes' => 'Codigo']);
    }
}
