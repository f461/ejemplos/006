<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

use yii\base\Model;

class ejercicio3  extends Model{
    
    
    public $numero1;
    public $numero2;
    public $numero3;
    
    
    public function attributeLabels() {
        return [
            "numero1" => "introduce numero1",
            "numero2" => "introduce numero2",
            "numero3" => "introduce numero3",
            
        ];
        
    }
    
    public function rules()[
        return[
            
            [['numero1','numero2','numero3'],'integer','message'=>'el campo{attribute} debe ser numero'],
            [['numero1','numero2','numero3'],'required'],
            ['numero3','compare','compareAttribute'=>'numero2','operator'=>'>']
            
        ];
        
        
    ]
    //put your code here
}
