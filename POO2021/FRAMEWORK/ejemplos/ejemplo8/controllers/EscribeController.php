<?php

namespace app\controllers;

use app\models\escribe;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EscribeController implements the CRUD actions for escribe model.
 */
class EscribeController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all escribe models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => escribe::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'autor' => SORT_DESC,
                    'libro' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single escribe model.
     * @param int $autor Autor
     * @param int $libro Libro
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($autor, $libro)
    {
        return $this->render('view', [
            'model' => $this->findModel($autor, $libro),
        ]);
    }

    /**
     * Creates a new escribe model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new escribe();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'autor' => $model->autor, 'libro' => $model->libro]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing escribe model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $autor Autor
     * @param int $libro Libro
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($autor, $libro)
    {
        $model = $this->findModel($autor, $libro);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'autor' => $model->autor, 'libro' => $model->libro]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing escribe model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $autor Autor
     * @param int $libro Libro
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($autor, $libro)
    {
        $this->findModel($autor, $libro)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the escribe model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $autor Autor
     * @param int $libro Libro
     * @return escribe the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($autor, $libro)
    {
        if (($model = escribe::findOne(['autor' => $autor, 'libro' => $libro])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
