<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\leer */

$this->title = 'Update Leer: ' . $model->autor;
$this->params['breadcrumbs'][] = ['label' => 'Leers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->autor, 'url' => ['view', 'autor' => $model->autor, 'libro' => $model->libro]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="leer-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        
        
        'listadoAutores'=>$listadoAutores,
            'listadoLibros' => $listadoLibros,
    ]) ?>

</div>
