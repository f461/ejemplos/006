<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\leer */

$this->title = 'Create Leer';
$this->params['breadcrumbs'][] = ['label' => 'Leers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="leer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'listadoAutores'=>$listadoAutores,
        'listadoLibros'=>$listadoLibros,
    ]) ?>

</div>
