<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\escribe */

$this->title = 'Update Escribe: ' . $model->autor;
$this->params['breadcrumbs'][] = ['label' => 'Escribes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->autor, 'url' => ['view', 'autor' => $model->autor, 'libro' => $model->libro]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="escribe-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
