<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\escribe */

$this->title = $model->autor;
$this->params['breadcrumbs'][] = ['label' => 'Escribes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="escribe-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'autor' => $model->autor, 'libro' => $model->libro], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'autor' => $model->autor, 'libro' => $model->libro], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'autor',
            'libro',
        ],
    ]) ?>

</div>
