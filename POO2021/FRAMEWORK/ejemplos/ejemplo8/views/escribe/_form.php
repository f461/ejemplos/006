<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\escribe */
/* @var $form yii\widgets\ActiveForm */

$autores = app\models\Autores::find()->all();
$libros = app\models\Libros::find()->all();

$listadoAutores= yii\helpers\ArrayHelper::map($autores, 'id', 'nombre');
$listadoLibros = yii\helpers\ArrayHelper::map($libros, 'id', 'nombre');
?>
<div class="escribe-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'autor')->dropDownList($listadoAutores)?>

    <?= $form->field($model, 'libro')->dropDownList($listadoAutores) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
