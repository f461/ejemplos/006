<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Autores y libros escritos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="escribe-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Escribe', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'autor',
            'autor0.nombre',
            'libro',
            'libro0.nombre',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
