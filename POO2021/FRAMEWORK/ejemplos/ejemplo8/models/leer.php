<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "leer".
 *
 * @property int $autor
 * @property int $libro
 *
 * @property Autores $autor0
 */
class leer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'leer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['autor', 'libro'], 'required'],
            [['autor', 'libro'], 'integer'],
            [['autor', 'libro'], 'unique', 'targetAttribute' => ['autor', 'libro']],
            [['autor'], 'exist', 'skipOnError' => true, 'targetClass' => Autores::className(), 'targetAttribute' => ['autor' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'autor' => 'Autor',
            'libro' => 'Libro',
        ];
    }

    /**
     * Gets query for [[Autor0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAutor0()
    {
        return $this->hasOne(Autores::className(), ['id' => 'autor']);
    }
}
