<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'informacion' => 'falso@gmail.com',
    'contacto' => 'contacto@gmail.com',
    'bsVersion' => '4.x',
];
