<?php

namespace app\models;

use yii\base\Model;
use Yii;

Class Contacto extends Model{
    public $nombre;
    public $email;
    public $telefono;
    public $direccion;
    public $fecha;
    public $politica;
    public $asunto;


    public function attributeLabels(): array {
        return[
            
            "nombre" => "Nombre completo",
            "email" => "Correo electronico",
            "telefono" => "Telefono",
            "direccion" => "Direccion completa",
            "asunto"=> "Indique pregunta",
            "fecha" => "fecha Nacimiento",
            "politica"=> "comprobarpoliticas",
            
            
        ];

}
  public function rules(){
      return[
          [["nombre","email","telefono"],"required"],
          ["email","email"],
          ["nombre","string","length" =>[1,100]],
          ["politica",'boolean'],
         // ["fecha", "date","format yyyy-mm-dd" ],
          ["fecha", "string" ],
        [["direccion", "asunto"], "safe"],  
          
          ["politica","comprobarpoliticas"], // chequear si has aceptado las politica
          
          
          
          
      ];
      
  }
  
  /**
   * 
   * chequear que se han aceptado.
   */
  public function comprobarpoliticas($attribute,$params){
      if($this->attributes==0){
          $this->addError($attribute,"Es necesarioaceptar politicas");
      }
  }
  
  public function contact()
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo(Yii::$app->params['contacto'])
                ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
                ->setReplyTo([$this->email => $this->nombre])
                ->setSubject("mandadon informacion")
                ->setTextBody($this->asunto)
                ->send();

            return true;
        }
        return false;
    }

}
          
      
      
      
  
      
        