
<?php
use yii\helpers\Html;
?>
<div class="row">
    <div class="col-lg-5">
    <?php
    echo Html::img("@web/imgs/$model->foto",[
    "class" =>"card-img-top"
    ]);
    
    ?>
    </div>
    <div class="col-lg-7">
        <h2><?= $model->nombre ?><br></h2>
        <div class="bg-warning rounded p-2">foto:</div>
        <div class="p-1"><?= $model->foto ?> foto</div>
        <div class="bg-warning rounded p-2">descripcion:</div>
        <div class="p-1"><?= substr ($model->descripcion,0,100) . "..." ?></div>
        <div class="bg-warning rounded p-2">Categoria:</div>
        <div class="p-1"><?= $model->categorias ?></div>
        
       <?= Html::a("Ver mas",["site/vermas","id"=>$model->id],["class"=>"Btn btn-primary float-right"])?>
            
         ?><br>
    </div>
    
</div>
