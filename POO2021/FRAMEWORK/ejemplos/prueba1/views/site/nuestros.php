<?php

use yii\bootstrap4\Carousel;
use yii\helpers\Html;

echo Carousel::widget([
    'items' => $fotos,
        'options'=> [
        'class'=>'mx-auto col-lg-8'

    ],
    'controls'=> ["<i class= fas fa-arrow-left fa-3x></i>","<i class= fas fa-arrow-right fa-3x></i>"]
]);
