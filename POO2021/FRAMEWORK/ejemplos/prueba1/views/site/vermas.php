<?php


use yii\widgets\DetailView;
use yii\helpers\Html;


echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',               // title attribute (in plain text)
        'description',
        'precio',
        'categoria',
        'oferta',
        
        [
          'label'=>'foto',
           'format'=>'Foto Producto',
            'value'=>function($data){
            $url ="@web/imgs/$data->foto";
            return Html::img($url,['style'=>'width:200px']);
    
    
            }
            
            
            
        ],
        'foto',// description attribute in HTML
        [                      // the owner name of the model
            'label' => 'Owner',
            'value' => $model->owner->name,
        ],
        'created_at:datetime', // creation date formatted as datetime
    ],
]);
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of vermas
 *
 * @author Programacion
 */

