<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use kartik\date\DatePicker;


/* @var $this yii\web\View */
/* @var $model app\models\Contacto */
/* @var $form ActiveForm */
?>
<div class="site-contacto">

        
    <?php $form = ActiveForm::begin(); ?>
<?= $form->errorSummary($model) ?>
        <?= $form->field($model, 'nombre')->input('') ?>
        <?= $form->field($model, 'email')->input('email') ?>
        <?= $form->field($model, 'telefono')->input('tel') ?>
 
        <?= $form->field($model, 'fecha')->input('date') ?>
    <?php    
            echo '<label class="control-label">'.$model->getAttributeLabel("fecha").'</label>';
            echo DatePicker::widget([
                'model' => $model, 
                'attribute' => 'fecha',
                'language' => 'es',
                'options' => ['placeholder' => 'Introduce fecha'],
                'pluginOptions' => [
                    'todayHighlight' => true,
                    'todayBtn' => true,
                    'format' => 'dd/mm/yyyy',
                    'autoclose' => true,
                ]
            ]);
        ?>
<?php
            //fecha sacada utilizadono directamente html5
            //echo $form-> field(model, "fecha")->input('date');
    ?>
        <?= $form->field($model, 'direccion') ?>
        <?= $form->field($model, 'asunto')->textarea(["rows"=>10]); ?>
        <?= $form->field($model, 'politica')->checkbox() ?>
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-contacto -->
