<?php
use yii\widgets\ListView;
?>

<h1 class="border rounded bg-secondary p-3 text-white text-center mb-5">Categorias</h1>
    
<?php
echo ListView::widget([
    "dataProvider" => $dataProvider,
    "itemView" => "_categorias",
    "itemOptions" => [
        'class' => 'col-lg-4',
    ],
    "options" => [
        'class' => 'row',
    ],
    'layout'=>"{items}"
]);
