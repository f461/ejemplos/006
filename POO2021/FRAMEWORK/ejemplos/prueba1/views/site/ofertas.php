<?php
use yii\widgets\ListView;
?>

<h1 class="border rounded bg-secondary p-3 text-white text-center mb-5">oferta</h1>
    <i class="fa fa-check" aria-hidden="true"> </i>


<?php
echo ListView::widget([
    "dataProvider" => $dataProvider,
    "itemView" => "_oferta",
    "options" => [
        'class' => 'row',
    ],
    'layout'=>"{items}",
    "itemOptions" => [
        'class' => 'col-lg-5 ml-auto mr-auto bg-light p-3 mb-5',
    ],

]);
