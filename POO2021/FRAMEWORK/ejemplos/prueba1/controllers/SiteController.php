<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Productos;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    /*public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }*/

    
    public function actionProductos() {
        $dataProvider=new ActiveDataProvider([
            'query'=> Productos::find()
              
     
        ]);
        
        return $this->render("productos",[
            "dataProvider" =>$dataProvider,
            
        ]);
        
    }
    
    public function actionNuestros() {
        $fotos= Productos::find()
            ->select("foto")
            ->where(["oferta"=>1])   
             ->asArray()
             ->all();
    
        
        $fotos= ArrayHelper::getColumn($fotos, "foto");
        
        $salida=[];
        foreach ($fotos as $src) {
            $salida[]=Html::img("@web/imgs/$src");
            
            
            
        }
        
        return $this->render("nuestros",[
            "fotos" =>$salida,
            
        ]);
        
    }
    
    public function actionOferta() {
        $dataProvider=new ActiveDataProvider([
            'query'=> Productos::find()->where(['oferta'=>1])
          
              
     
        ]);
         return $this->render("ofertas",[
            "dataProvider" =>$dataProvider,
             
             ]);
         
    }
          public function actionCategorias(){
        $dataProvider=new ActiveDataProvider([
            'query' => Productos::find()->select('categorias')->distinct()
        ]);
        
        return $this->render("categorias",[
            "dataProvider" => $dataProvider,
        ]);
        
          }
         public function actionVer($categoria){
        $productos=Productos::find()->where(['categorias'=>$categoria]);
       
        
        
        $dataProvider=new ActiveDataProvider([
            'query'=> $productos, 
        ]);
        
        return $this->render('ver',[
            'dataProvider'=>$dataProvider,
            'categoria'=>$categoria
        ]);
    }

        public function actionDonde(){
        return $this->render("donde");
        
        }
            
        public function actionQuienes(){
        return $this->render("quienes");
        }
        
        
        public function actionProductos2(){
        return $this->render("productos2");
        }
        
        
        public function actionContacto()
{
    $model = new \app\models\Contacto(); //objeto tipo contacto vacio
    
    
    
    if ($model->load(Yii::$app->request->post())) { /// leo los datos y enviado a datos
        if ($model->contact()) { // model es objeto  
            if($model->politica){
           
            return $this->render("Mostrar");
        }
    }
    }
    return $this->render('contacto', [
        'model' => $model,
    ]);
        
    }
    
     public function actionVermas($id){
    $salida=Productos::find()->where(["id"=>$id]);
     $salida=Productos::findOne($id);
     
     
     return $this->render("vermas",[
         "model" => $salida,
         
         
         
     ]);
} 
    /**
     * Displays about page.
     *
     * @return string
     */
   /*public function actionAbout()
    {
        return $this->render('about');
    }
    
    
    */


}