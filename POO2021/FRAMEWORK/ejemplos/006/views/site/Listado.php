<?php
use yii\grid\GridView;
use yii\helpers\Html;


echo GridView::widget([
    "dataProvider" => $registro,
    "columns" => [
        
        'id',
        'titulo',
        'Descripcion',
        'Fecha_estreno',
       
       
         [
            'label'=>'Portada',
            'format'=>'raw',
            'value' => function($data){
               $url="@web/imgs/$data->Portada";
               
              return Html::img($url,["style"=>"width:200px"]);
            }
        ],
        
        
        [
            'label'=>'Categoria',
            'format'=>'raw',
            'value' => function($data){
                return Html::a($data->Categoría,['site/vergrid',"id"=>$data->id],["class"=>'btn btn-info']); 
            }
        ],
        
         
  
            
            
            
        
  
    ]
  
]);
        ?>