<?php

//ver Peliculas de categoria

use yii\widgets\DetailView;
use yii\helpers\Html;
echo DetailView::widget([
    
    
    "Model"=> $Categoria,
    "attibutes"=>[
        'id',
        'titulo',
        'descripcion',
        'duracion',
        'fecha_Estreno',
        'Categoria',
        'Director',
        'Destacada',
        'pelliculaSemana',
         [
            'label'=>'Portada',
            'format'=>'raw',
            'value' => function($data){
                $url = '@web/imgs/' . $data->Portada;
                return Html::img($url,['class'=>'img-fluid','style'=>'width:300px']); 
            }
        ],
        
        
    ]
    
    
]);
