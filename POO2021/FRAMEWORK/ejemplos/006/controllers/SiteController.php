<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Listado;
use app\models\Categoria;
use app\models\Destacadas;
use app\models\Pelicula;



class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionInicio()
    {
        $peliculas= Pelicula::find()
                ->where(['Pelicula_de_la_semana'=>1])
                ->one();
        return $this->render('index',[
            'pelicula' => $peliculas->Portada,
            'id'=>$peliculas->id,
            
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
   public function actionListado() {
        $registro=new ActiveDataProvider([ 
          "query" => Pelicula::find(),
            
        ]);
        
        return $this ->render("Listado",[
           "registro" => $registro, 
 
        ]);

    }
    
    public function actionDestacadas() {
        $registro=new ActiveDataProvider([ 
          "query" => Pelicula::find()->where(['destacadas'=>1]),
            
        ]);
        
        return $this ->render("Destacadas",[
           "dataProvider" => $registro, 
 
        ]);

    }
    
     public function actionCategorias() {
        $registro=new ActiveDataProvider([ 
          "query" => Pelicula::find()->select('categoria')->distinct(),
            
        ]);
        
        return $this ->render("Categoria",[
           "registro" => $registro, 
 
        ]);

    }
    
    
    // accion que me muestra la peliculas sobre la que he realizado clic 
    public function actionVerpelicula($id) {
       $pelicula= Peliculas::findOne($id);
       
       return $this->render("verPelicula",[
           "pelicula" => $pelicula
           
           
       ]);
    }
    
    //accion que me muestra las peliculas de una determianda categoria
    /**
     * seleccionada en la vista Categoria
     * @param type $categoria categoria selecionada
     * @return type vista donde se muestra 
     * 
     */
    
    public function actionVer($categoria) {
        $registro=new ActiveDataProvider([
            "query"=> Pelicula::find()->where(['categoria'=>$categoria]),
            
              ]);
      
               return $this->render('ver',[
            "dataProvider" => $categoria,
              ]);
        
        
    }
    
    /**
     * Accion que me muestra las peliculasl de uan determinada categoria
     * selecciona en la vista listado
     * @param type $id de la pelicula selecionnada
     * @return vista vergrid que me muestra las peliculas de la categoria de la pelicula seleccionada Description
     * 
     */
    
    public function actionVergrid($id){
        
        //saco la categoria de la pelicula sobre la que se hace clic 
        $categoria=Pelicula::findOne($id)->Categoría;
        
            $dataProvider=new ActiveDataProvider([
                "query"=>Pelicula::find()
                
                
            ]); 
            
            return $this->render ("vergrid", [
            "dataprovider"=>$dataProvider        
            ]);
            
            
    }
    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
