DROP DATABASE IF EXISTS ejemplo10yii;
CREATE DATABASE ejemplo10yii;
USE ejemplo10yii;


CREATE TABLE fotos (
  
  codigo int AUTO_INCREMENT,
  nombre varchar (100),
PRIMARY KEY (codigo)
 
  );





  CREATE TABLE noticias (

  codigo int AUTO_INCREMENT,
  texto varchar(1000),
  fecha date,
  PRIMARY KEY (codigo)
);


    CREATE TABLE comentarios (
      codigo int AUTO_INCREMENT,
      texto varchar(1000),
      fecha date,
      cod_noticias int NOT null,
      
      PRIMARY KEY (codigo) ,
     Constraint fk_comentarios_noticias FOREIGN KEY (cod_noticias) REFERENCES noticias(codigo)
      ON DELETE CASCADE ON UPDATE CASCADE
      );


  
 CREATE TABLE noticias_fotos (
  codfo int ,
  cofnoti int ,
  visitas  int DEFAULT 0,
  PRIMARY KEY (codfo,cofnoti),
  CONSTRAINT fk_noticiasfoto_noticias FOREIGN KEY (cofnoti) REFERENCES  noticias(codigo)
  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_noticiasfotos_fotos FOREIGN KEY (codfo) REFERENCES  fotos(codigo)
  ON DELETE CASCADE ON UPDATE CASCADE

);