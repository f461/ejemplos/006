<?php

use yii\helpers\Html;
use yii\bootstrap4\Carousel;


?>
    <div class="col-lg-12">
        <h2><?= $model->titulo ?><br></h2>
        <div class="bg-warning rounded p-2">Fecha:</div>
        <div class="p-1"><?= $model->fecha ?> minutos</div>
        <div class="bg-warning rounded p-2">Contenido</div>
        <div class="p-1 mb-4"><?= $model->texto ?></div>
        <div class="mb-5">
        <?php
        
        //sacar las fotos de esa noticia
            $fotos=$model->getCodFos()->all();
            //en este array quiero colocar todas las fotos
            $elementos=[];
            foreach ($fotos as $foto) {
               $elementos[]=Html::img("@web/imgs/" . $foto->nombre);
                
            }
            //pregunto si esa noticia tiene alguna foto
        if (count($elementos)==0){
            // en caso de que tenga foto coloco un caroousel con todas
            echo Html::img("@web/imgs/nada.jpg",[
                "class"=> 'mx-auto col-lg-10'
                
                
            ]);
            
            
        }else{
        
        
        echo Carousel::widget([
          
         'items'=> $elementos 
            
        
        ]);
        }
        
        ?>
        </div>
        <div class="clearfix mb-3">
        <?=  
           Html::a("añadir comentario",
        ['site/nuevocomentario','codigo'=>$model->codigo],
        ['class'=>'btn btn-primary float-right col-lg-5']
        );
           //boton1 
         ?>
        <?=  

           //boton 2
           Html::a("Ver comentarios",
        ['site/vercomentarios','codigo'=>$model->codigo],
        ['class'=>'btn btn-primary float-right col-lg-5']
        );   
         ?>
            <div class="clearfix mb-3">
             <?=  

           //boton 3
           Html::a("editarnoticia",
        ['site/editarnoticia','codigo'=>$model->codigo],
        ['class'=>'btn btn-primary float-right col-lg-5']
        );   
         ?><br>
         
             <?= 
           Html::a("eliminarnoticia",
        ['site/confirmareliminarnoticia','codigo'=>$model->codigo],
        ['class'=>'btn btn-primary float-right col-lg-5']
        );   
         ?>
            
        
            <br>
            </div>
    
        </div>
</div>
