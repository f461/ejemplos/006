<?php
use yii\widgets\ListView;
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Noticias';
?>
<div class="site-index">
<h1 class="border rounded bg-secondary p-3 text-white text-center mb-5">Listado de peliculas Destacadas</h1>

    <div class="body-content">
     <?=  

           //boton 2
           Html::a("Añadir noticia",
        ['site/nuevanoticia'],
        ['class'=>'btn btn-primary float-right']
        );   
         ?>
        
    </div>
        <?= 

        ListView::widget([
            'dataProvider'=>$dataProvider,
            'itemView' => '_noticia',
            
            "itemOptions" => [
        'class' => 'col-lg-12 ml-auto mr-auto bg-light p-3 mb-5',
    ],
    "options" => [
        'class' => 'row',
    ],
    'layout'=>"{items}"

            
        ]);
       
?>
       
       

                
            </div>
        </div>

  
