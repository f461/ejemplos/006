<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Noticias */
/* @var $form ActiveForm */
?>
<div class="editarnoticia">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'fecha') ?>
        <?= $form->field($model, 'titulo') ?>
        <?= $form->field($model, 'texto') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- editarnoticia -->
