<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Noticias;
use app\models\Fotos;
use app\models\Comentarios;
use yii\helpers\Html;



class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        
        
        
        $consulta=Noticias::find();
        $fotos=Fotos::find();// muestra todas las fotos de la base de datos.
        $dataProvider = new ActiveDataProvider([
            'query' =>$consulta,
            
            
        ]
    
   
);
return $this->render('index',['dataProvider'=> $dataProvider
        
        ]);

    }
    
    public function actionNuevocomentario($codigo){
           $model = new Comentarios();
           
          

    if ($model->load(Yii::$app->request->post())) {
        $model->cod_noticias=$codigo;
        if ($model->save()) {
            // form inputs are valid, do something here
            return $this->redirect(["vercomentarios","codigo"=>$codigo]);
        }
    }

    return $this->render('formulario', [
        'model' => $model,
    ]);
}
        
        
    
    public function actionVercomentarios($codigo){
        
        $consulta= Comentarios::find()->where(["cod_noticias"=>$codigo]);
        $noticia=Noticias::findOne($codigo);
        $fotos=$noticia->getCodfos()->all();
        
        
        $dataProvider = new ActiveDataProvider([
    'query' => $consulta,
   
]);
        return $this->render("listar",[
            "dataProvider" => $dataProvider,
            "noticia" => $noticia,
            "fotos" => $fotos,
            
            ]);
    }
    
    

    
    
    public function actionEditarcomentario($codigo) {
        /*
         * Cargar los datos del comentarios desde la base de datos
         */
        
        $comentario= Comentarios::findOne($codigo);
        /*
         * quiero saber si he pulsado el boton editar desde el listado de comentarios 
         * o al boton modificar del formulario de actualizacion del comentario.
         */
        if($this->request->isPost){
            
            
            // aqui llego si he pulsado el boton de editar en el formulario del comentario
        
        
        if ($comentario->load(Yii::$app->request->post())) { //cargo los datos del formulario en el modelo
        $comentario->cod_noticias=$codigo;
        if ($comentario->save()) {
           
            // aqui solo llega si he plsado el boton de editar comnetario en el listado de comentario
            
            return $this->redirect(["vercomentarios","codigo"=>$comentario->cod_noticias]);
        }
    }
    
        }
        
        return $this->render('formulario', [
        'model' => $comentario,
        ]);
    }
    
    
    public function actionEliminarcomentario($codigo) {
        $comentario= Comentarios::findOne($codigo);
        
        
        //obtengo la noticia del comentario a eliminar.
       $cod_noticias=$comentario->cod_noticias;
       
        //eliminar el comentario
        $comentario->delete();
                return $this->redirect(["vercomentarios","codigo"=>$cod_noticias]);
    }
    
    
    
    /**
     * Login action.
     *
     * @return Response|string
     */
    

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionNuevanoticia(){
        
        $model= new Noticias; // modelo para almacenar los datos; Aqui se pregunta si vengo del formulario;
        
        
        
         if ($model->load(Yii::$app->request->post())) {
        
        if ($model->save()) { //intentado grabar lso datos en la tabla
            // form inputs are valid, do something here
            return $this->redirect(["site/index"]); //cargo la pagina principal
        }
    }
 //cargo el formulario porque es la primera vex que llamas a la accion o porque hay erroes en los fallos
    return $this->render('formulario_1', [
        'model' => $model,
    ]);
}
public function actionEditarnoticia($codigo) {
        /*
         * Cargar los datos del comentarios desde la base de datos
         */
        
        $noticia= Noticias::findOne($codigo);
        /*
         * quiero saber si he pulsado el boton editar desde el listado de comentarios 
         * o al boton modificar del formulario de actualizacion del comentario.
         */
        if($this->request->isPost){
            
            
            // aqui llego si he pulsado el boton de editar en el formulario del comentario
        
        
        if ($noticia->load(Yii::$app->request->post())) { //cargo los datos del formulario en el modelo
       
        if ($noticia->save()) {
           
            // aqui solo llega si he pulsado el boton de editar comnetario en el listado de comentario
            
            return $this->redirect(["index"]);
         }
        }
    
        }
        
        return $this->render('editarnoticia', [
        'model' => $noticia,
        ]);
    }
    
    public function actionEliminarnoticia($codigo) {
        $noticia= Noticias::findOne($codigo);
        
        
        //obtengo la noticia del comentario a eliminar.
      
       
        //eliminar el comentario
        $noticia->delete();
                return $this->redirect(["index"]);
    }
         
    public function actionConfirmarEliminarnoticia($codigo) {
        $noticia= Noticias::findOne($codigo);
        
        
        //obtengo la noticia del comentario a eliminar.
      
       
        //eliminar el comentario
        
                return $this->render("site/ver",["model"=> $noticia
                        ]);
    }
    }


