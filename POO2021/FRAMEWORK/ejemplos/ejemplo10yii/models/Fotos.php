<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fotos".
 *
 * @property int $codigo
 * @property string|null $nombre
 *
 * @property Noticias[] $cofnotis
 * @property NoticiasFotos[] $noticiasFotos
 */
class Fotos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fotos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Cofnotis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCofnotis()
    {
        return $this->hasMany(Noticias::className(), ['codigo' => 'cofnoti'])->viaTable('noticias_fotos', ['codfo' => 'codigo']);
    }

    /**
     * Gets query for [[NoticiasFotos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNoticiasFotos()
    {
        return $this->hasMany(NoticiasFotos::className(), ['codfo' => 'codigo']);
    }
}
