<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comentarios".
 *
 * @property int $codigo
 * @property string|null $texto
 * @property string|null $fecha
 * @property int $cod_noticias
 *
 * @property Noticias $codNoticias
 */
class Comentarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comentarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['cod_noticias'], 'required'],
            [['cod_noticias'], 'integer'],
            [['texto'], 'string', 'max' => 1000],
            [['cod_noticias'], 'exist', 'skipOnError' => true, 'targetClass' => Noticias::className(), 'targetAttribute' => ['cod_noticias' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'texto' => 'Texto',
            'fecha' => 'Fecha',
            'cod_noticias' => 'Cod Noticias',
        ];
    }

    /**
     * Gets query for [[CodNoticias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodNoticias()
    {
        return $this->hasOne(Noticias::className(), ['codigo' => 'cod_noticias']);
    }
}
