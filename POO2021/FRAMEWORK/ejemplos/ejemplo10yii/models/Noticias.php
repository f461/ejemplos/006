<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "noticias".
 *
 * @property int $codigo
 * @property string|null $titulo
 * @property string|null $texto
 * @property string|null $fecha
 *
 * @property Fotos[] $codfos
 * @property Comentarios[] $comentarios
 * @property NoticiasFotos[] $noticiasFotos
 */
class Noticias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['titulo'], 'string', 'max' => 100],
            [['texto'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'titulo' => 'Titulo',
            'texto' => 'Texto',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * Gets query for [[Codfos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodfos()
    {
        return $this->hasMany(Fotos::className(), ['codigo' => 'codfo'])->viaTable('noticias_fotos', ['cofnoti' => 'codigo']);
    }

    /**
     * Gets query for [[Comentarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComentarios()
    {
        return $this->hasMany(Comentarios::className(), ['cod_noticias' => 'codigo']);
    }

    /**
     * Gets query for [[NoticiasFotos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNoticiasFotos()
    {
        return $this->hasMany(NoticiasFotos::className(), ['cofnoti' => 'codigo']);
    }
}
