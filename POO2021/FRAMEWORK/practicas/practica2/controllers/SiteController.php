<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\widgets\DetailView;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
   
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionListar1() 
    {
     $s=Entradas::find()-all();
     
     return $this->render("listarTodos",
             [
                 
                 "datos"=>$s
                 
             ]);
    }
    
    public function actionListar2()
    {
     $salida=Entradas::find()->select(['texto'])->asArray()->all();
     return $this->render('listarTodos',["datos"=>$salida]);
    }

    public function actionListar3()
    {
        $salida=Entradas::find()->select(['texto'])->all();
        return $this->render('listarTodos',["datos"=>$salida]);
        
    }
            
    public function actionListar4()
    {
        $salida=new Entradas();
        return $this->render('listarTodos',[
            "datos"=>$salida->find()->all()
                ]);
        
    }
    
    public function actionListar5()
    {
        $salida=new Entradas();
        
        return $this->render('listarTodos',[
            "datos"=>$salida->findOne(1),
  
        ]);
        
        
    }
    
    public function actionListar()
    {
     return $this->render('listarTodos',[
         "datos"=>Yii::$app->db->createCommand("Select * from entradas")->queryAll(),   
     ]);
    }
    
    public function actionMostraruno()
    {
        return $this->render('mostrarUno',[
            'model'=> Entradas::findOne(1),
            
            
        ]);
        
        
    }
    
    
    public function actionMostar()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Entradas::find(),
            
        ]);
        
        return $this->render('mostrar', [
            'dataProvider' => $dataProvider,
        ]);
        
        
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }
  
}