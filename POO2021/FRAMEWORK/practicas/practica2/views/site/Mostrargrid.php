<?php

use yii\grid\GridView;

?>

<?=GridView::widget([
    'dataProvider' => $dataProvider,
    'summary'=>"Mostrando {begin} - {end} de {totalCount} elementos",
    'colums' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id',
        'texto',
    ],

]);?>

