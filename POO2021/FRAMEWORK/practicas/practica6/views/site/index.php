<?php

use yii\widgets\ListView;
?>

<?= Listview::widget([
        'dataProvider' => $dataProvider,
        'itemView' =>'_index',
        "itemOptions" => [
        'class' => 'col-lg-5 ml-auto mr-auto bg-light p-3 mb-5',
    ],
    "options" => [
        'class' => 'row',
    ],
    'layout'=>"{items} {pager}<br>",
        "pager"=>[
        "options" => ["class"=>"row col-6"],
        "linkOptions"=>["class"=>"p-2 d-block w-100 h-100"],
        "disabledPageCssClass"=>"p-2 rounded d-block col-1 border",
        "activePageCssClass"=>"bg-light",
        "nextPageCssClass"=>"rounded d-block col-1 border",
        "prevPageCssClass"=>"rounded d-block col-1 border",
        "pageCssClass"=>"rounded d-block col-1 p-0 border",
    ],

    ]); ?>
