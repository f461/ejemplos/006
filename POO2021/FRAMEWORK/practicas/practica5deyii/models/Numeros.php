<?php



namespace app\models;

use yii\base\Model;

class Numeros extends Model{
    public $numero1;
    public $numero2;
    public $tipo;
    public $sumar;
    public $restar;
    public $multiplicar;
    public $dividir;
    
    public function attributeLabels(){
        return[
            "numero1"=>"Numero 1",
            "numero2"=>"Numero 2",
            "sumar"=> "la suam es",
            "restar"=> "la resta es",
            "multiplicarr"=> "la multi es",
            "dividir"=> "la divi es",
            
            
        ];
            
    }
    
    public function rules(){
        return [
            [['numero1','numero1'],'integer'],
            [['numero1','numero2'],'required'],
          /*  ['numero2', 'compare', 'compareValue' => 0, 'operator' => '!=', 'type' => 'number','when'=>function($model){
            $model->tipo=="dividir"; //solo relaiza la validad si la operacion es dividir.
            }]*/
            ['numero2', 'dividirPorCero'],
            
    ];
    }
    /**
     * esta regla me comprueba cuando estoy diviendo que el numero2  no sea 0
     * @param type $atributo
     * @param type $parametros
     */
    public function dividirPorCero($atributo,$parametros){
        if($this->tipo=="dividir" || $this->tipo=="todo"){
            if($this->numero2==0){
                $this->addError("numero2", "No puede ser un 0 si se divide");
                
            }
            
        }
    }
            
    public function operacion (){
        switch ($this->tipo){
            
            case 'sumar':
                return $this -> numero1+$this->numero2;
                break;
            case 'restar':
                return $this -> numero1-$this->numero2;
                break;
             case 'multiplicar':
                return $this -> numero1*$this->numero2;
                break;
             case 'dividir':
                return $this -> numero1/$this->numero2;
                break;
            default:
                return 0;
        }
        
        
    }
    
    public function operarTodo(){
        
        $this->sumar=$this->numero1+$this->numero2;
        $this->restar=$this->numero1-$this->numero2;
        $this->multiplicar=$this->numero1*$this->numero2;
        $this->dividir=$this->numero1/$this->numero2;
        
    }
}
