<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
   

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    Public function actionMensaje(){
        return $this->render('Vista'); // Renderizo la vista
    }
     Public function actionMeses(){
       return $this->render('meses');   
        
    }
    /**
     * Login action.
     *
     * @return Response|string
     */
    

    /**
     * Logout action.
     *
     * @return Response
     */
   

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    

    /**
     * Displays about page.
     *
     * @return string
     */
}
