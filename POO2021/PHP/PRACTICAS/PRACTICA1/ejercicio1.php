<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<!--David Sanchez Cayon 29/06/2021-->

<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio1</title>
    </head>
    <body>
        <div>Es texto escrito directamente en HTML</div>
        <?php
        // Comentario de linea
        echo "texto escrito desde PHP<br>";
        /*
         * Comentario de varias lineas
         */
        print "Texto escrito desde PHP";
        
        ?>
        <h1>Segunda parte</h1>
        <?php
            #Comentario de una linea
        $a=10; #variable de tipo entero
        echo "Escribir en PHP para colocar el valor de la variable $a";
        ?>
    </body>
</html>
