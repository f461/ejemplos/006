<!DOCTYPE html>
<!--
David Sanchez ejemplo 13 imagenes aleatorias.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $fotos=[
            'bici','bobina','caballito','cascos','comida','edificio','lunes'
        ];
        $numerofotos=count($fotos);
        $indice= mt_rand(0,$numerofotos -1);
        $foto=$fotos[$indice];
        ?>
        
        <img src="imgs/<?= $foto?>.jpg" alt="foto bici"/>
    </body>
</html>
