<?php
    //include "./ejemplo3/Persona.php";
    //require "./ejemplo3/Persona.php";
    //include_once './ejemplo3/Persona.php';
    require_once './ejemplo3/Persona.php';
    require_once './ejemplo3/Animal.php';
    
?>

<?php
    $alumno=new Persona();
    $perro=new Animal();
    
    $alumno->setNombre("Andres");
    echo "Mi alumno se llama " . $alumno->getNombre();
    
    $perro->setTipo("perro");
    $perro->setNombre("Tor");
    
    $perro->datos();
    
?>