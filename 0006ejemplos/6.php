<?php 
    class Objeto{
        public $tipo;
        public static $color="negro";
    }

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $caja=new Objeto();
            $caja->tipo="caja";
            var_dump($caja);
            //$caja->color="gris"; // esto no lo permite porque es un miembro estatico
            //$caja::$color="gris"; // funciona pero no es recomendable
            Objeto::$color="gris";
            echo Objeto::$color;
            
            
            
            $mueble=new Objeto();
            $mueble->tipo="mueble";
            var_dump($mueble);
            echo $mueble::$color;
            
            
        
        ?>
    </body>
</html>
