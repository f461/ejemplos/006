<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Animal
 *
 * @author Programacion
 */

namespace ejemplo8;
class Animal {
    public $nombre;
    private $raza;
    protected $color;
    public $fechaNacimiento;
    
    public function getNombre() {
        return $this->nombre;
    }

    public function getRaza() {
        return $this->raza;
    }

    public function getColor() {
        return $this->color;
    }

    public function getFechaNacimiento() {
        return $this->fechaNacimiento;
    }

    public function setNombre($nombre): void {
        $this->nombre = $nombre;
    }

    public function setRaza($raza): void {
        $this->raza = $raza;
    }

    public function setColor($color): void {
        $this->color = $color;
    }

    public function setFechaNacimiento($fechaNacimiento): void {
        $this->fechaNacimiento = $fechaNacimiento;
    }





}


public static  function ladrido (){
    
    return "guau";
}
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $perro=new Objeto();
            $perro->tipo="perro";
            var_dump($perro);
            //$caja->color="gris"; // esto no lo permite porque es un miembro estatico
            //$caja::$color="gris"; // funciona pero no es recomendable
            
            Objeto::$raza="buldog";
            echo Objeto::$raza;
            
            
            
            $mueble=new Objeto();
            $mueble->tipo="mueble";
            var_dump($mueble);
            echo $mueble::$color;
            
            
        
        ?>
    </body>
</html>
