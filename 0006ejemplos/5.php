<?php
spl_autoload_register(function ($nombre_clase) {
    include $nombre_clase . '.php';
});

use ejemplo5\Animal;
use ejemplo5\Persona;
?>

<?php
    $alumno=new Persona();
    $perro=new Animal();
    
    $alumno->setNombre("Andres");
    echo "Mi alumno se llama " . $alumno->getNombre();
    
    $perro->setTipo("perro");
    $perro->setNombre("Tor");
    
    $perro->datos();
    
?>