<!DOCTYPE html>
<?php


function spl_autoload_register($nombre_clase){
    include "ejercicio1\\" . $nombre_clase . '.php';
     
}

use ejercicio1\Vehiculo;
use ejercicio1\Camion;
use ejercicio1\Autobus;

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $camion = new Camion();
        $camion->encender();
        $camion->cargar(10);
        $camion->verificar_encendido(); 
        $camion->matricula = 'MDU - 293';
        $camion->apagar();
        $autobus = new Autobus ();
        $autobus->encender();
        $autobus->subir_pasajero (5);
        $autobus->verificar_encendido ();
        $autobus->matricula = 'KDF - 923';
        $autobus->apagar();
        ?>
    </body>
</html>

