<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            #tabla{
                display:table;
                border-collapse: separate;
                border-spacing: 10px;
            }
            #tabla>div{
                display:table-cell;
                width:50px;
                height: 50px;
                text-align: center;
                vertical-align: middle;
                font-size: 2em;
                color:#ccc;
                border: 1px solid black;
            }
            
            #posicion{
                background-image: url('./imgs/circle.svg');
                background-size: 100% 100%;
            }
        </style>
    </head>
    <body>
        <?php
        $numero= mt_rand(1,6); # numero aleatorio entre 1 y 6
        ?>
        <img src="./imgs/<?= $numero?>.svg">
        
        <div id="tabla">
        <?php
        for($c=0;$c<6;$c++){
            if($c==($numero-1)){ // si el contador es igual al numero del dado menos 1
        ?>
            <div id="posicion"><?= $c+1 // esta celda es la que tiene la ficha?></div> 
        <?php
            }else{
         ?>
            <div><?= $c+1 // esto son las celdas sin ficha ?></div>
         <?php    
            }
        ?>
        
        <?php
        }
        ?>
        </div>
        
    </body>
</html>
