<!DOCTYPE html>
<?php
/**
 *@author david Sanchez Cayon
 * @date 26/07/2021
 *  */

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form action="ejercicio5Destino.php">
            <div>
                <label for="Nombre">
                    Nombre:
                    </label>
                <input type="text" name="nombre" id="Nombre" placeholder="Introduce tu nombre">
            </div>
            <br>
            <div>
                <label for="Apellido">
                    apellido:
                </label>
                <input type="text" id="apellidos" name="apellido" placeholder="Introduce tu apellido">
            </div>  
            <br>  
            <div>
                <label for="Codigo">
                    Codigo postal:
                </label>
                <input type="number" id="Codigo" name="Codigo" placeholder="Introduce tu codigo postal">
            </div> 
            <br>
            <div>
                <label for="Telefono">
                    telefono:
                </label>
                <input type="number " id="telefono" name="telefono" placeholder="Introduce tu telefono">
            </div> 
            <br>
            <div>
                <label for="correo">
                    Correo:
                </label>
                <input type="email" id="correo" name="correo" placeholder="Introduce tu correo">
            </div> 
            <div>
                <button formmethod="GET">Enviar </button>
                
            </div>
        </form>
    </body>
</html>
